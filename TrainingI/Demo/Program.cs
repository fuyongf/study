﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Demo
{
    class Program
    {
        static void Main(string[] args)
        {
         
            // 数据
            var users = new[] { 
                new { Name = "user1", Company = 1 },
                new { Name = "user2", Company = 2 },
                new { Name = "user3", Company = 3 },
                new { Name = "user4", Company = 4 },
            };

            var companies = new[] {
                new { Id = 1, Name = "IBM" },
                new { Id = 2, Name = "Microsoft" }
            };

            //执行流程分析
            var ints = new int[] { 1, 2, 3, 4, 5, 6 };
            var q1 = from i in ints where i > 3 select i;
            q1.ToList().ForEach(Console.WriteLine);

            // 交叉连接
            Console.WriteLine("交叉连接");
            var q2 = from u in users
                     from c in companies
                     where u.Company == c.Id
                     select new { Name = u.Name, Company = c.Name };

            foreach (var i in q2)
            {
                Console.WriteLine("{0}, {1}", i.Name, i.Company);
            }

            // 内连接
            Console.WriteLine("内连接");
            var q3 = from u in users
                     join c in companies
                     on u.Company equals c.Id
                     select new { Name = u.Name, Company = c.Name };

            foreach (var i in q3)
            {
                Console.WriteLine("{0}, {1}", i.Name, i.Company);
            }

            // 外连接
            Console.WriteLine("外连接");
            var q4 = from u in users
                     join c in companies
                     on u.Company equals c.Id
                     into uc
                     from x in uc.DefaultIfEmpty()
                     select new { Name = u.Name, Company = x == null ? "unknown" : x.Name };

            foreach (var i in q4)
            {
                Console.WriteLine("{0}, {1}", i.Name, i.Company);
            }

            // 分组

            var data = new[] 
            {
                new { 分类 = "日本车", 汽车品牌 = "本田"},
                new { 分类 = "日本车", 汽车品牌 = "丰田"},
                new { 分类 = "国产车", 汽车品牌 = "奇瑞"},
                new { 分类 = "国产车", 汽车品牌 = "中华"},
                new { 分类 = "国产车", 汽车品牌 = "长城"},
                new { 分类 = "德国车", 汽车品牌 = "大众"},
                new { 分类 = "美国车", 汽车品牌 = "福特"},
            };

            var query = (from car in data
                         group car by car.分类 into g
                         select new
                         {
                             分类 = g.Key,
                             品牌数量 = g.Count()
                         }).Where(a => a.品牌数量 > 1).OrderByDescending(a => Guid.NewGuid()).Skip(1).Take(1);

            query.ToList().ForEach(a => Console.WriteLine("{0}:{1}", a.分类, a.品牌数量));


            // 字符串

            string text = @"Historically, the world of data and the world of objects " +
       @"have not been well integrated. Programmers work in C# or Visual Basic " +
       @"and also in SQL or XQuery. On the one side are concepts such as classes, " +
       @"objects, fields, inheritance, and .NET Framework APIs. On the other side " +
       @"are tables, columns, rows, nodes, and separate languages for dealing with " +
       @"them. Data types often require translation between the two worlds; there are " +
       @"different standard functions. Because the object world has no notion of query, a " +
       @"query can only be represented as a string without compile-time type checking or " +
       @"IntelliSense support in the IDE. Transferring data from SQL tables or XML trees to " +
       @"objects in memory is often tedious and error-prone.";

            string[] sentences = text.Split(new char[] { '.', '?', '!' });
            var wordsToMatch = Enumerable.Empty<string>().ToList();
            var sentenceQuery = from sentence in sentences
                                let w = sentence.Split(new char[] { ' ', ',', '?', '!' }, StringSplitOptions.RemoveEmptyEntries)
                                where w.Distinct().Intersect(wordsToMatch).Count() == wordsToMatch.Count()
                                select sentence;
            wordsToMatch.AddRange(new[] { "Historically", "data", "integrated" }); // 再次演示延迟执行特性
            foreach (string str in sentenceQuery)
            {
                Console.WriteLine(str);
            }



            // 反射


            Assembly assembly = Assembly.Load("System.Core, Version=3.5.0.0, Culture=neutral, PublicKeyToken= b77a5c561934e089");
            var pubTypesQuery = from type in assembly.GetTypes()
                                where type.IsPublic
                                from method in type.GetMethods()
                                where method.ReturnType.IsArray == true
                                    || (method.ReturnType.GetInterface(
                                        typeof(System.Collections.Generic.IEnumerable<>).FullName) != null
                                    && method.ReturnType.FullName != "System.String")
                                group method.Name.ToString() by type.ToString();

            foreach (var groupOfMethods in pubTypesQuery)
            {
                Console.WriteLine("Type: {0}", groupOfMethods.Key);
                foreach (var method in groupOfMethods)
                {
                    Console.WriteLine("  {0}", method);
                }
            }

            // IO

            string startFolder = @"e:\program files\Microsoft Visual Studio 9.0\";

            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(startFolder);

            IEnumerable<System.IO.FileInfo> fileList = dir.GetFiles("*.*", System.IO.SearchOption.AllDirectories);

            string searchTerm = @"Visual Studio";

            var queryMatchingFiles =
                from file in fileList
                where file.Extension == ".htm"
                let fileText = GetFileText(file.FullName) // 完全可以引入自己的方法
                where fileText.Contains(searchTerm)
                select file.FullName;

            Console.WriteLine("The term \"{0}\" was found in:", searchTerm);
            foreach (string filename in queryMatchingFiles.Take(5))
            {
                Console.WriteLine(filename);
            }

            // 扩展

            var items = new[] { "a", "b", "c" };
            Console.WriteLine(items.GetSingleRandom());

            Console.ReadLine();
        }

        static string GetFileText(string name)
        {
            string fileContents = String.Empty;

            if (System.IO.File.Exists(name))
            {
                fileContents = System.IO.File.ReadAllText(name);
            }
            return fileContents;
        }


    }

    public static class Extenders
    {
        public static T GetSingleRandom<T>(this IEnumerable<T> target)
        {
            int seed = int.Parse(new string((from c in Guid.NewGuid().ToString()
                                             where char.IsDigit(c)
                                             select c).Take(int.MaxValue.ToString().Length - 1).ToArray()));
            Random r = new Random(seed);
            int position = r.Next(target.Count());
            return target.ElementAt<T>(position);
        }
    }
}
