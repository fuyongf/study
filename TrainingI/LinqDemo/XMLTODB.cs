﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LinqDemo
{
    class XMLTODB
    {
        public static void Start()
        {
            // 在这样的代码中看到的全是业务逻辑，看不到ADO.NET悠长的代码，也看不到Xml解析悠长的代码

            using (DBDataContext db = new DBDataContext())
            {
                db.ExecuteCommand("truncate table [Order]");
                db.ExecuteCommand("truncate table [OrderDetail]");
            }

            var data = (from o in XDocument.Load("data.xml").Descendants("Orders").Descendants("Order")
                        let customer = o.Descendants("Customer").FirstOrDefault()
                        let products = o.Descendants("Details").Descendants("Product")
                        select new
                        {
                            CustomerName = customer.Attribute("Name").Value,
                            CustomerTel = customer.Attribute("Tel").Value,
                            Details = (from p in products
                                       select new
                                       {
                                           ProductName = p.Attribute("Name").Value,
                                           ProductPrice = int.Parse(p.Attribute("Price").Value),
                                           ProductCount = p.Attribute("Count") == null ? 1 : int.Parse(p.Attribute("Count").Value),
                                       }).ToList()
                        }).ToList(); // linq to xml

            using (DBDataContext db = new DBDataContext())
            {
                foreach (var o in data)
                {
                    var order = new Order
                    {
                        CustomerName = o.CustomerName,
                        CustomerTel = o.CustomerTel,
                        ItemCount = o.Details.Count,
                        TotalPrice = o.Details.Sum(d => d.ProductCount * d.ProductPrice),
                    }; // linq to object


                    db.Order.InsertOnSubmit(order);
                    db.SubmitChanges();
                    db.OrderDetail.InsertAllOnSubmit(from od in o.Details
                                                     select new OrderDetail
                                                     {
                                                         OrderID = order.ID,
                                                         ProductCount = od.ProductCount,
                                                         ProductName = od.ProductName,
                                                         ProductPrice = od.ProductPrice
                                                     });
                    db.SubmitChanges(); // linq to sql
                }
            }

            using (DBDataContext db = new DBDataContext())
            {
                var result = db.Order.OrderByDescending(o => o.TotalPrice).Select(o =>
                    new
                    {
                        Name = o.CustomerName,
                        Price = o.TotalPrice
                    }).FirstOrDefault();
                Console.WriteLine(string.Format("数据挖掘，最有钱的是{0}，它的订单总额是{1}元", result.Name, result.Price.Value.ToString("N0")));
            }

            Console.ReadLine();
        }
    }
}
