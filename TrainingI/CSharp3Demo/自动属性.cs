﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;

namespace CSharp3Demo
{
    public class Person
    {
        private string _name;

        public string Name
        {
            get
            {
                return this._name;
            }

            set
            {
                this._name = value;
            }
        }
    }

    public class Person2
    {
        public string Name
        {
            get; 
            set;
        }
    }

    public class Person3
    {
        public Person3(string name)
        {
            this.Name = name;
        }

        public string Name
        {
            get;
            private set;
        }
    }

    public class Person4
    {
        public string Name { get; set; }

        public int Age { get; set; }

        public Address Address { get; set; }

        //public string GetProfile()
        //{
        //    return Name;
        //}
    }

    public class Address
    {
        public string City { get; set; }
        public string Street { get; set; }
    }

}
