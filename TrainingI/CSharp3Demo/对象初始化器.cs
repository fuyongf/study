﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CSharp3Demo
{
    [TestClass]
    public class 对象初始化器
    {
        [TestMethod]
        public void 对象初始化器测试()
        {
            Person4 ppp = new Person4();
            ppp.Name = "zhuye";
            ppp.Age = 28;
            Address ad = new Address();
            ad.City = "aaa";
            ad.Street = "bbb";

            Person4 pp = new Person4()
            {
                Name = "zhuye",
                Age = 28,
                Address = new Address 
                {
                    City = "aaa", 
                    Street = "bbb"
                }
            };

            Person4 p = new Person4 { Name = "zhuye" , Age = 28 };
            Assert.AreEqual(p.Name, "zhuye");
        }
    }
}
