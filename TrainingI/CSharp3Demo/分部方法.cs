﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace CSharp3Demo
{
    [TestClass]
    public class 分部方法
    {
        [TestMethod]
        public void 分部方法测试()
        {
            test.foo();
            // 方法是否编译进去了？
            Assert.IsNotNull(typeof(test).GetMethod("log", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic));
        }
    }

    public partial class test
    {
        static partial void log();

        public static void foo()
        {
            log();
        }
    }

    public partial class test
    {
        
        //static partial void log()
        //{
        //    Debug.WriteLine("分部方法测试");
        //}
    }
}
