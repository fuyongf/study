﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace CSharp3Demo
{
    [TestClass]
    public class 匿名类型
    {
        [TestMethod]
        public void 匿名类型测试()
        {
            var a = new
            {
                Name = "zhuye",
                Age = 29,
                Address = new
                {
                    City = "Shanghai"
                }
            };
            Assert.AreEqual(a.Address.City, "Shanghai");
            Debug.WriteLine("匿名类型测试" + a.GetType().Name);

            var b = new
            {
                Name = "zhuye", 
                Age = 29
            };
            var bb = new
            {
                Name = "sadasdsad",
                Age = 29
            };
            Assert.AreEqual<Type>(bb.GetType(), b.GetType());

            var c = new
            {
                Age = 29,
                Name = "dsfsdfdf",
            };
            // 属性位置变化了
            Assert.AreNotEqual<Type>(a.GetType(), c.GetType());

            var d = new
            {
                Name = "zhuye",
                Age = 29
            };
            // 重写了Equals
            Assert.AreEqual(b, d);
        }
    }
}
