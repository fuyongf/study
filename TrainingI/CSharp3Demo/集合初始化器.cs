﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace CSharp3Demo
{
    [TestClass]
    public class 集合初始化器
    {
        [TestMethod]
        public void 集合初始化器测试()
        {
            List<Person> dd = new List<Person>();
            dd.Add(new Person { Name = "a" });
            dd.Add(new Person { Name = "a" });

            List<Person> persons = new List<Person>()
            {
                new Person { Name = "a"},
                new Person { Name = "b"}
            };
            Assert.AreEqual(persons.Count, 2);

            Dictionary<string, int> data = new Dictionary<string, int>()
            {
                { "aa", 28 },
                { "bb", 27 }
            };
            Assert.AreEqual(data["aa"], 28);

            // 自定义支持集合初始化器的类型
            PersonCollection pc = new PersonCollection()
            {
                { "aa", 28 },
                { "bb", 27 }
            };

            Assert.AreEqual(pc.l[0].Name, "aa");

        }
    }

    class PersonCollection : IEnumerable
    {
        public List<Person4> l = new List<Person4>();

        #region IEnumerable 成员

        public IEnumerator GetEnumerator()
        {
            return l.GetEnumerator();
        }

        #endregion

        public void Add(string name, int age)
        {
            l.Add(new Person4 { Age = age, Name = name });
        }
    }
}
