﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CSharp3Demo
{
    [TestClass]
    public class 查询句法
    {
        [TestMethod]
        public void 查询句法测试()
        {
            var query = (from p in new[]
            {
                new { Name = "Abc", Price = 20 }, 
                new { Name = "Bcd", Price = 30 },
                new { Name = "Cde", Price = 60 }, 
            }
                         where p.Price < 50
                         orderby p.Price descending
                         select new
                         {
                             ProductName = p.Name.ToUpper(),
                             ProductPrice = p.Price
                         }).FirstOrDefault(); //一句语句？

            Assert.AreEqual(query.ProductName, "BCD");

            // 没有集合初始化器
            List<Product> data = new List<Product>();
            Product p1 = new Product();
            // 没有对象初始化器
            p1.Name = "Abc";
            p1.Price = 20;
            data.Add(p1);
            Product p2 = new Product();
            p2.Name = "Bcd";
            p2.Price = 30;
            data.Add(p2);
            Product p3 = new Product();
            p3.Name = "Cde";
            p3.Price = 60;
            data.Add(p3);

            IEnumerable<Product> q1 = Enumerable.Where(data, delegate(Product p) //没有扩展方法
            {
                return p.Price < 50;
            });
            q1 = Enumerable.OrderByDescending(q1, delegate(Product p)
            {
                return p.Price;
            });

            IEnumerable<NewProduct> q2 = Enumerable.Select<Product, NewProduct>(q1, delegate(Product p) //没有lambda表达式
            {
                NewProduct np = new NewProduct(); // 没有匿名方法
                np.ProductPrice = p.Price;
                np.ProductName = p.Name.ToUpper();
                return np;
            });

            Assert.AreEqual(q2.FirstOrDefault().ProductName, "BCD");
        }


    }

    class Product
    {
        private string name;
        private int price;
        //没有自动属性
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public int Price
        {
            get
            {
                return price;
            }
            set
            {
                price = value;
            }
        }
    }

    class NewProduct
    {
        public string ProductName
        {
            get;
            set;
        }
        public int ProductPrice
        {
            get;
            set;
        }
    }


}
