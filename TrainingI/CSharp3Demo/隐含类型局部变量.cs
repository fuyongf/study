﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CSharp3Demo
{
    [TestClass]
    public class 隐含类型局部变量
    {
        [TestMethod]
        public void 隐含类型局部变量测试()
        {
            var a = 1;
            var b = 1.0;
            var c = "Mark";
            var d = null as Uri;
            var e = default(IEnumerable<Person>);
            Assert.IsInstanceOfType(a, typeof(Int32));
            Assert.IsInstanceOfType(b, typeof(Double));
            Assert.IsInstanceOfType(c, typeof(String));

            int i = 0;
            foreach (var item in new Dictionary<int, int> { { 1, 1 }, { 2, 2 } })
            {
                i += item.Key;
            }
            Assert.AreEqual(i, 3);
        }


        //var a = "a";

        //private var Action()
        //{
        //    throw new NotImplementedException();
        //}

        //private void Action(var paramter)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
