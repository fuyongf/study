﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CSharp3Demo
{
    [TestClass]
    public class 扩展方法
    {
        [TestMethod]
        public void 扩展方法测试()
        {
            Person4 p = new Person4 { Name = "a", Age = 1 };
            Assert.AreEqual(p.GetProfile(), "b 2"); // 测试扩展方法的优先级
            Assert.AreEqual("a".ToMD5(), "0CC175B9C0F1B6A831C399E269772661");
            Assert.IsTrue(1.In(1, 2));

            var f = from x in 234324
                    select x + 1;
            Assert.AreEqual(f(1),2);
        }
    }

    public static class Extension
    {
        public static string GetProfile(this Person4 p)
        {
            //Person4 p = this;
            p.Name = "b";
            p.Age++;
            return string.Format(p.Name + " " + p.Age);
        }

        public static string ToMD5(this string s)
        {
            return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(s, "md5");
        }

        public static bool In<T>(this T t, params T[] c)
        {
            return c.Any(i => i.Equals(t));
        }

        public static Func<int, TR> Select<TR>(this int i, Func<int, TR> func)
        {
            return func;
        }
    }
}
