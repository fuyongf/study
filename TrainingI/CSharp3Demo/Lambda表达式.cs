﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using System.Diagnostics;

namespace CSharp3Demo
{
    [TestClass]
    public class Lambda表达式
    {
        [TestMethod]
        public void Lambda表达式测试()
        {
            // 最普通的用方法初始化委托
            var IsPositive = new Func<int, bool>(this.IsPositive);
            IsPositive(1);

            // 用匿名方法初始化委托
            Func<int, bool> IsPositive1 = delegate(int num)
            {
                return num > 0;
            };

            // 用lambda表达式初始化委托
            Func<int, bool> IsPositive2 = (n) =>
            {
                return n > 0;
            };

            // 省略return
            Func<int, bool> IsPositive3 = (n) => n > 0;

            // 省略括号
            Func<int, bool> IsPositive4 = n => n > 0;

            // 测试方法
            Assert.IsTrue(new Func<int, bool>(n => n > 0)(1));

            // 典型应用
            Thread t = new Thread(() =>
            {
                Thread.Sleep(100);
                Debug.WriteLine("Lambda表达式测试");
            });
            t.Start();
            t.Join();

            int a = 1;
            int b = 2;
            Stopwatch sw = Stopwatch.StartNew();
            NomalMethod(a, ExpensiveCalculation(b));
            // 很明显这个方法需要100毫秒
            Assert.IsTrue(sw.ElapsedMilliseconds > 50);
            // 后面那个方法不会执行，执行时间几乎为0
            int c = LazyEvaluationMethod(() => a, () => ExpensiveCalculation(b));
            // 总的运行时间略微超过100毫秒
            Assert.IsTrue(sw.ElapsedMilliseconds < 110);
            Assert.AreEqual(a, c);

            int d;
            // out参数的lambda表达式
            new OutHandler((out int dd) => dd = 3)(out d);
            Assert.AreEqual(d, 3);

            // 返回方法的方法
            Func<int, Func<int, int>> f = x => y => x + y;
            Assert.AreEqual(f(1)(2), 3);


        }

        delegate void OutHandler(out int x);

        private bool IsPositive(int num)
        {
            return num > 0;
        }

        static int NomalMethod(int a, int b)
        {
            return a > 0 ? a : b;
        }

        static int LazyEvaluationMethod(Func<int> a, Func<int> b)
        {
            return a() > 0 ? a() : b();
        }

        static int ExpensiveCalculation(int a)
        {
            Thread.Sleep(100);
            return a * 2;
        }
    }
}
