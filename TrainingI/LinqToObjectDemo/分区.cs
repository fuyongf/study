﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace LinqToObjectDemo
{

    [TestClass]
    public class 分区
    {
        [TestMethod]
        public void Take()
        {
            var items = new[] { 10, 20, 30, 40, 30, 20, 10 };
            Assert.AreEqual(items.Take(2).LastOrDefault(), 20);
            // TakeWhile
            Assert.AreEqual(items.TakeWhile(i => i < 40).Count(), 3);
        }

        [TestMethod]
        public void Skip()
        {
            var items = new[] { 10, 20, 30, 40, 30, 20, 10 };
            Assert.IsTrue(items.Skip(2).Take(2).SequenceEqual(new[] { 30, 40 }));
            // SkipWhile
            Assert.AreEqual(items.SkipWhile(i => i < 40).Count(), 4);
        }
    }
}
