﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LinqToObjectDemo
{
    [TestClass]
    public class 分组
    {
        [TestMethod]
        public void GroupBy()
        {
            var scores = new[]
            {
                new { Name = "a", Score = 90 },
                new { Name = "b", Score = 80 },
                new { Name = "c", Score = 70 },
                new { Name = "d", Score = 60 },
                new { Name = "e", Score = 40 },
                new { Name = "f", Score = 30 },
            };

            Assert.AreEqual(scores.GroupBy(a =>
            {
                if (a.Score >= 80)
                    return "好";
                else if (a.Score >= 60)
                    return "中";
                else
                    return "差";
            }).Count(), 3);

            Assert.AreEqual(scores.GroupBy(a =>
            {
                if (a.Score >= 80)
                    return "好";
                else if (a.Score >= 60)
                    return "中";
                else
                    return "差";
            }, a => a.Score).Select(g => g.Sum()).First(), 170);

            Assert.AreEqual(scores.GroupBy(a =>
            {
                if (a.Score >= 80)
                    return "好";
                else if (a.Score >= 60)
                    return "中";
                else
                    return "差";
            }).Where(g => g.Key == "好").Select(g => g.Average(a => a.Score)).Single(), 85);
        }
    }
}
