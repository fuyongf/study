﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace LinqToObjectDemo
{
    [TestClass]
    public class 转换
    {
        [TestMethod]
        public void ToDictionary()
        {
            var data = new[]
            {
                new { ID = 1, Name = "A", B = 1},
                new { ID = 2, Name = "B", B = 2},
                new { ID = 3, Name = "B", B = 1}
            };
            Assert.AreEqual(data.ToDictionary(d => d.ID, d => d.Name)[1], "A");
        }

        [TestMethod]
        public void ToLookup()
        {
            var data = new[]
            {
                new { ID = 1, Name = "A", B = 1},
                new { ID = 2, Name = "B", B = 2},
                new { ID = 3, Name = "B", B = 1}
            };

            var result = data.ToLookup(d => d.Name, d => d.B);
            foreach (var item in result)
            {
                Debug.WriteLine(item.Key);
                foreach (var subitem in item)
                    Debug.WriteLine("\t" + subitem);
            }

            Assert.IsTrue(data.ToLookup(d => d.Name, d => d.B)["B"].ToArray().SequenceEqual(new[] { 2, 1 }));

        }

        [TestMethod]
        public void Other()
        {
            var data = new object [] { 1, "a" };
            // Cast
            Assert.IsInstanceOfType(data.Cast<int>().FirstOrDefault(), typeof(int));
            // ToArray
            Assert.AreEqual(Enumerable.Repeat(1, 10).ToArray().Length, 10);
            // ToList
            Assert.AreEqual(Enumerable.Repeat(1, 10).ToList().Count, 10);
        }
    }
}
