﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LinqToObjectDemo
{
    [TestClass]
    public class 排序
    {
        [TestMethod]
        public void OrderBy()
        {
            var data = new[]
            {
                new { ID = 1, Name = "A", B = 1},
                new { ID = 2, Name = "B", B = 2},
                new { ID = 3, Name = "B", B = 1}
            };
            Assert.AreEqual(data.OrderByDescending(a => a.Name).OrderBy(a => a.B).LastOrDefault().ID, 2);
            Assert.AreEqual(data.OrderByDescending(a => a.Name).ThenBy(a => a.B).LastOrDefault().ID, 1);
        }

        [TestMethod]
        public void Reverse()
        {
            var data = new[] { 1, 2, 3 };
            Assert.AreEqual(data.Reverse().FirstOrDefault(), 3);
        }
    }
}
