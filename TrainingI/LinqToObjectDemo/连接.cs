﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace LinqToObjectDemo
{
    [TestClass]
    public class 连接
    {
        [TestMethod]
        public void CrossJoin()
        {
            var groupA = new[] { "汉堡", "批萨" };
            var groupB = new[] { "牛肉", "猪肉", "鸡肉" };

            groupA.SelectMany(a => groupB, (a, b) => b + a).ToList().ForEach(a => Debug.WriteLine(a));
            Assert.AreEqual(groupA.SelectMany(a => groupB, (a, b) => b + a).FirstOrDefault(), "牛肉汉堡");
        }

        [TestMethod]
        public void Join()
        {
            var courses = new[]
            {
                new { ID = 1, Name = "高等数学" },
                new { ID = 2, Name = "编译原理" }
            };
            var students = new[]
            {
                new { ID = 1, Name = "干叔叔" },
                new { ID = 2, Name = "高工" }
            };
            var scores = new[]
            {
                new { CourseID = 1, StudentID = 1, Score = 70 },
                new { CourseID = 2, StudentID = 1, Score = 80 },
                new { CourseID = 1, StudentID = 2, Score = 75 },
                new { CourseID = 2, StudentID = 2, Score = 85 },
            };

            var results = scores.Join(courses, score => score.CourseID, course => course.ID, (score, course) => new
            {
                CourseName = course.Name,
                StudentID = score.StudentID,
                Score = score.Score
            }).Join(students, score => score.StudentID, student => student.ID, (score, student) => new
            {
                CourseName = score.CourseName,
                StudentName = student.Name,
                Score = score.Score
            }).ToList();

            Assert.AreEqual(results.Where(a => a.CourseName == "高等数学" && a.StudentName == "高工").SingleOrDefault().Score, 75);

        }

        [TestMethod]
        public void GroupJoin()
        {
            var courses = new[]
            {
                new { ID = 1, Name = "高等数学" },
                new { ID = 2, Name = "编译原理" },
                new { ID = 3, Name = "7S管理方法" }
            };

            var studentCourses = new[]
            {
                new { CourseID = 1, StudentName = "高工" },
                new { CourseID = 2, StudentName = "干叔叔" },
                new { CourseID = 2, StudentName = "高工" },
            };

           var result = courses.GroupJoin(studentCourses, course => course.ID, studentCourse => studentCourse.CourseID, (course, students) => new
            {
                CourseName = course.Name,
                StudentName = students.Select(s=>s.StudentName).ToList()
            }).ToList();

           Assert.IsTrue(result.Any(d => d.CourseName == "7S管理方法"));
           Assert.AreEqual(result.Where(d => d.CourseName == "编译原理").SelectMany(d => d.StudentName).Count(), 2);
        }
    }
}
