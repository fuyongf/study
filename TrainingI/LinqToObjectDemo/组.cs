﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LinqToObjectDemo
{
    [TestClass]
    public class 组
    {
        [TestMethod]
        public void Set()
        {
            var groupA = new[] { "a", "b", "c", "b" };
            var groupB = new[] { "c", "d", "e", "a" };

            // 顾虑相同项
            Assert.IsTrue(groupA.Distinct().ToArray().SequenceEqual(new[] { "a", "b", "c" }));

            // 连接，不过滤相同项
            Assert.IsTrue(groupA.Concat(groupB).ToArray().SequenceEqual(new[] { "a", "b", "c", "b", "c", "d", "e", "a" }));

            // 合并，过滤相同项
            Assert.IsTrue(groupA.Union(groupB).ToArray().SequenceEqual(new[] { "a", "b", "c", "d", "e" }));

            // 交集
            Assert.IsTrue(groupA.Intersect(groupB).ToArray().SequenceEqual(new[] { "a", "c" }));
 
            // 排除
            Assert.IsTrue(groupA.Except(groupB).ToArray().SequenceEqual(new[] { "b" }));

            // 配对联合
            Assert.IsTrue(groupA.Zip(groupB, (first, second) => first + second).SequenceEqual(new[] { "ac", "bd", "ce", "ba"}));

        }

    }

    static class SequenceExtension
    {
        public static IEnumerable<TResult> Zip<T1, T2, TResult>(
                this IEnumerable<T1> source1,
                IEnumerable<T2> source2, Func<T1, T2, TResult> func)
        {
            using (var iter1 = source1.GetEnumerator())
            using (var iter2 = source2.GetEnumerator())
            {
                while (iter1.MoveNext() && iter2.MoveNext())
                {
                    yield return func(iter1.Current, iter2.Current);
                }
            }
        }
    }
}
