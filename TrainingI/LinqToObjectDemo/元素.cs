﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LinqToObjectDemo
{
    [TestClass]
    public class 元素
    {
        [TestMethod]
        public void Element()
        {
            var data = new[] { 3, 1, 2 };
            // First
            Assert.AreEqual(data.First(), 3);
            // Last
            Assert.AreEqual(data.Last(), 2);
            // FirstOrDefault
            Assert.AreEqual(data.FirstOrDefault(n => n > 3), 0);
            // LastOrDefault
            Assert.AreEqual(data.LastOrDefault(n => n < 3), 2);
            // SingleOrDefault
            Assert.AreEqual(data.SingleOrDefault(n => n == 1), 1);
            // Where
            Assert.AreEqual(data.Where(n => n == 1).Single(), 1);
            // ElementAt
            Assert.AreEqual(data.ElementAt(1), 1);
            // ElementAtOrDefault
            Assert.AreEqual(data.ElementAtOrDefault(3), 0);
            // First
            Assert.AreEqual(data.Where(n => n > 3).DefaultIfEmpty(-99).First(), -99);
        }
    }
}
