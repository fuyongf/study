﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LinqToObjectDemo
{
    [TestClass]
    public class 限定
    {
        [TestMethod]
        public void Any()
        {
            var d = new[] { 1, 2, 3 };
            Assert.IsTrue(d.Any(n => n % 2 == 0));
        }

        [TestMethod]
        public void All()
        {
            var d = new[] { "a", "aa", "abb" };
            Assert.IsTrue(d.All(n => n.StartsWith("a")));
        }

        [TestMethod]
        public void Contain()
        {
            var d = new[] { "a", "aa", "abb" };
            Assert.IsTrue(d.Contains("AA", new LambdaComparer<string>((a, b) => a.ToLower() == b.ToLower())));
        }
    }

    public class LambdaComparer<T> : IEqualityComparer<T>
    {
        private readonly Func<T, T, bool> _lambdaComparer;
        private readonly Func<T, int> _lambdaHash;

        public LambdaComparer(Func<T, T, bool> lambdaComparer) :
            this(lambdaComparer, o => o.ToString().ToLower().GetHashCode())
        {
        }

        public LambdaComparer(Func<T, T, bool> lambdaComparer, Func<T, int> lambdaHash)
        {
            if (lambdaComparer == null)
                throw new ArgumentNullException("lambdaComparer");
            if (lambdaHash == null)
                throw new ArgumentNullException("lambdaHash");

            _lambdaComparer = lambdaComparer;
            _lambdaHash = lambdaHash;
        }

        public bool Equals(T x, T y)
        {
            return _lambdaComparer(x, y);
        }

        public int GetHashCode(T obj)
        {
            return _lambdaHash(obj);
        }
    }
}
