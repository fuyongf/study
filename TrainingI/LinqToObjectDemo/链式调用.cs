﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Collections;

namespace LinqToObjectDemo
{
    [TestClass]
    public class 链式调用
    {
        [TestMethod]
        public void MethodChain()
        {
            人 p = new 人() { Name = "张三" };
            p.吃("饭").喝("水").拉();
            
            
        }
    }


    public class 人 : I人
    {
        public string Name
        {
            get;
            set;
        }
    }

    public interface I人
    {
        string Name { get; set; }
    }

    public static class ext
    {
        public static I人 吃(this I人 person, string food)
        {
            Debug.WriteLine(string.Format("{0}吃了{1}", person.Name, food));
            return person;
        }

        public static I人 喝(this I人 person, string food)
        {
            Debug.WriteLine(string.Format("{0}喝了{1}", person.Name, food));
            return person;
        }

        public static I人 拉(this I人 person)
        {
            Debug.WriteLine(string.Format("{0}拉了", person.Name));
            return person;
        }
    }
}
