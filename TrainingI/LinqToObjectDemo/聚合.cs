﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LinqToObjectDemo
{
    [TestClass]
    public class 聚合
    {
        [TestMethod]
        public void Aggregate()
        {
            var data = new[] { "Word", "Good", "Better", "Best" };
            Assert.AreEqual(data.Aggregate((itema, itemb) => string.Format("{0},{1}", itema, itemb)), "Word,Good,Better,Best");
            Assert.AreEqual(data.Aggregate(0, (wordLength, item) => wordLength + item.Length), 18);
            Assert.AreEqual(data.Aggregate(0, (wordLength, item) => wordLength + item.Length, totalLength => "totalLength:" + totalLength), "totalLength:18");
        }

        [TestMethod]
        public void Other()
        {
            var data = new[] { 1, 2, 3, 4 };
            // Count
            Assert.AreEqual(data.Where(num=>num >2).Count(), 2);
            // Sum
            Assert.AreEqual(data.Sum(), 10);
            // LongCount
            Assert.IsInstanceOfType(data.LongCount(), typeof(Int64));
            // Min
            Assert.AreEqual(data.Min(), 1);
            // Max
            Assert.AreEqual(data.Max(), 4);
            // Average
            Assert.AreEqual(data.Average(), 2.5);
        }
    }
}
