﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace LinqToObjectDemo
{
    [TestClass]
    public class 延迟执行
    {
        [TestMethod]
        public void DeferredExecution()
        {
            Debug.WriteLine("延迟执行");
            var num = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var i = 0;

            var q = num.Select(n => ++i);
            foreach (var n in q)
            {
                Debug.WriteLine(string.Format("n = {0}; i = {1}", n, i));
            }
        }

        [TestMethod]
        public void EagerExecution()
        {
            Debug.WriteLine("非延迟执行");
            var num = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var i = 0;

            var q = num.Select(n => ++i).ToList();
            foreach (var n in q)
            {
                Debug.WriteLine(string.Format("n = {0}; i = {1}", n, i));
            }
            
        }

        //public static class Enumerable
        //{
        //    public static List<TSource> ToList<TSource>(this IEnumerable<TSource> source)
        //    {
        //        return new List<TSource>(source);
        //    }
        //}

        //public class List<T> : IList<T>, ICollection<T>, IEnumerable<T>, IList, ICollection, IEnumerable
        //{
        //    public List(IEnumerable<T> collection)
        //    {
        //        ICollection<T> is2 = collection as ICollection<T>;
        //        if (is2 != null)
        //        {
        //            int count = is2.Count;
        //            this._items = new T[count];
        //            is2.CopyTo(this._items, 0);
        //            this._size = count;
        //        }
        //        else
        //        {
        //            this._size = 0;
        //            this._items = new T[4];
        //            using (IEnumerator<T> enumerator = collection.GetEnumerator())
        //            {
        //                while (enumerator.MoveNext())
        //                {
        //                    this.Add(enumerator.Current);
        //                }
        //            }
        //        }
        //    }
        //} 
    }
}
