﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LinqToObjectDemo
{

    [TestClass]
    public class 投影
    {
        [TestMethod]
        public void Select()
        {
            var data = new[]
            {
                new { Name = "a", Age = 50 },
                new { Name = "b", Age = 60 }
            };
            Assert.AreEqual(data.Select(a => a.Age).Average(), 55);
        }

        [TestMethod]
        public void SelectMany()
        {
            var data = new[]
            {
                new { Items = new[] { new { Price = 1 }, new { Price = 2 }}},
                new { Items = new[] { new { Price = 3 }, new { Price = 4 }}}
            };
            Assert.AreEqual(data.SelectMany(a => a.Items).Select(a => a.Price).Sum(), 10);
        }
    }
}
