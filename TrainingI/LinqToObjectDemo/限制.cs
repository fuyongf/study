﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LinqToObjectDemo
{
    [TestClass]
    public class 限制
    {
        [TestMethod]
        public void Where()
        {
            var data = new[] { 1, 2, 3, 1.1, 2.2, 3.3 };

            Assert.AreEqual(data.Where(d => d > 2).Count(), 3);
            Assert.AreEqual(data.Where((d, index) => d > 2 && index % 2 == 0).Count(), 2);
        }

        [TestMethod]
        public void OfType()
        {
            var data = new object[] { 1, 2, 3, 1.1, 2.2, 3.3 };
            Assert.AreEqual(data.OfType<Int32>().Where(d => d > 2).Count(), 1);
        }
    }
}
