﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;

namespace LinqToXmlDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            // 声明方式创建xml
            var doc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XComment("创建测试"),

                new XElement("Orders",
                    new XElement("Order",
                        new XElement("Customer",
                            new XAttribute("Name", "干叔叔"),
                            new XAttribute("Tel", "要三八xxxxxxxxx")),
                       new XElement("Details",
                            new XElement("Product",
                                new XAttribute("Name", "摄像头1"),
                                new XAttribute("Price", "80"),
                                new XAttribute("Conut", "10")),
                            new XElement("Product",
                                new XAttribute("Name", "摄像头2"),
                                new XAttribute("Price", "100"),
                                new XAttribute("Conut", "10")))),
                    new XElement("Order",
                        new XElement("Customer",
                            new XAttribute("Name", "高工"),
                            new XAttribute("Tel", "要三酒xxxxxxxxx")),
                       new XElement("Details",
                            new XElement("Product",
                                new XAttribute("Name", "海景房A"),
                                new XAttribute("Price", "1000000")),
                            new XElement("Product",
                                new XAttribute("Name", "海景房A"),
                                new XAttribute("Price", "2000000"))))
                                ));

            doc.Save("data.xml");

            // 把立体的结构变为扁平的结构，演示Ancestors/Descendants
            var doc2 = XDocument.Load("data.xml");
            var q = from p in
                        (from order in doc2.Element("Orders").Elements("Order")
                         where order.Element("Customer").Attribute("Name").Value == "干叔叔"
                         select order).SelectMany(a => a.DescendantsAndSelf("Product"))
                    select new
                    {
                        PName = p.Attribute("Name").Value,
                        PPrice = p.Attribute("Price").Value,
                        CName = p.Ancestors("Order").SingleOrDefault().Element("Customer").Attribute("Name").Value
                    };
            q.ToList().ForEach(a => Console.WriteLine("{0}买了价值{1}的{2}", a.CName, a.PPrice, a.PName));
    
            // 演示xpath/设置/替换/删除xml
            var doc3 = XDocument.Parse(
                            @"<people>
                                <person>
                                    <id>1</id>
                                    <name>user1</name>
                                    <age>22</age>
                                </person>
                                 <person>
                                    <id>2</id>
                                    <name>user2</name>
                                    <age>33</age>
                                </person>
                            </people>");

            doc3.XPathSelectElements("/people/person[id=1]").SingleOrDefault().Element("name").Value = "修改";
            doc3.XPathSelectElements("/people/person[id=1]").SingleOrDefault().Element("name").ReplaceWith(new XElement("address", "asdsad"));
            doc3.Descendants("age").Remove();
            Console.WriteLine(doc3.ToString());
            Console.ReadLine();

        }
    }
}
