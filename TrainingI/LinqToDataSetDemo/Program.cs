﻿/* http://www.cnblogs.com/lovecindywang
** 朱晔 @ 2010/9
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace LinqToDataSetDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SqlConnection conn = new SqlConnection("Data Source=.;Initial Catalog=linqtest;Integrated Security=True"))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("select * from [order]; select * from [orderdetail]", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ds.Relations.Add("OrderDetails", ds.Tables[0].Columns["ID"], ds.Tables[1].Columns["OrderID"]);
                Console.WriteLine(ds.Tables[0].AsEnumerable().Where(a => a.Field<string>("CustomerName") == "高工").SingleOrDefault().Field<string>("CustomerTel"));
                var data = (from o in ds.Tables[0].AsEnumerable()
                            where o.Field<string>("CustomerName").Contains("叔叔")
                            select new
                            {
                                Name = o.Field<string>("customername"),
                                AveragePrice = o.GetChildRows("OrderDetails").Average(od => od.Field<decimal>("ProductPrice"))
                            }).Single();
                Console.WriteLine(string.Format("{0}:{1}", data.Name, data.AveragePrice));
            }
            Console.ReadLine();
        }
    }
}
