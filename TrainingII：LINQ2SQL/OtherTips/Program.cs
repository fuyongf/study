﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using System.Data.Linq;
using System.Data.Common;

namespace OtherTips
{
    class Program
    {
        static Func<AdventureWorksDataContext, string, IOrderedQueryable<Customer>> CustomersByCompany =
            CompiledQuery.Compile((AdventureWorksDataContext db, string name) => from c in db.Customers
                                                                                 where c.CompanyName.Contains(name)
                                                                                 orderby c.CompanyName descending
                                                                                 select c);

        static void Main(string[] args)
        {
            "已编译查询".HightLightWriteLine();
            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                Console.WriteLine(CustomersByCompany(db, "Bikes").Count());
            }

            "显式打开连接".HightLightWriteLine();
            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                db.Connection.StateChange +=
                    (o, s) => Console.WriteLine("Original:{0} -> CurrentState:{1}", s.OriginalState, s.CurrentState);

                db.Connection.Open();
                db.ProductModels.InsertOnSubmit(new ProductModel { Name = Guid.NewGuid().ToString(), ModifiedDate = DateTime.Now, rowguid = Guid.NewGuid() });
                Console.WriteLine(db.ProductModels.Count());
                db.SubmitChanges();
                db.Connection.Close();
            }

            "探究查询".HightLightWriteLine();
            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                var query = db.Customers.Where(c => c.Title == "Mr.");
                DbCommand cmd = db.GetCommand(query);
                Console.WriteLine(cmd.CommandText);
                foreach (DbParameter parm in cmd.Parameters)
                    Console.WriteLine(parm.ParameterName + ":" + parm.Value);
                query.First().Title = "Ms.";
                var updated = db.GetChangeSet().Updates;
                Console.WriteLine((updated.First() as Customer).Title);
            }
        }
    }
}
