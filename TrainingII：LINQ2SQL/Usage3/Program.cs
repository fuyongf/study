﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;

namespace Usage3
{
    class Program
    {
        static void Main(string[] args)
        {
            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                int? adresscount = 0;
                db.sp_withparameter(29503, ref adresscount);
                adresscount.Value.ToString().HightLightWriteLine("具有输出参数的存储过程");

                db.sp_withreturnvalue(10).ToString().HightLightWriteLine("具有返回值的存储过程");

                db.sp_singleresultset().First().FirstName.HightLightWriteLine("返回单个结果集的存储过程");

                "返回多个结果集的存储过程".HightLightWriteLine();
                var result = db.sp_multiresultset2();
                Console.WriteLine(result.GetResult<Customer>().First().FirstName);
                Console.WriteLine(result.GetResult<CustomerAddress>().First().Address);

                "演示返回标量值的自定义函数".HightLightWriteLine();
                var query = from o in db.SalesOrderHeaders
                            select new
                            {
                                CustomerName = string.Format("{0} {1}", o.Customer.FirstName, o.Customer.LastName),
                                Products = o.SalesOrderDetails.Select(order => order.Product.Name),
                                StatusText = db.ufnGetSalesOrderStatusText(o.Status)
                            };
                query.Take(2).ToList().ForEach(r =>
                     {
                         Console.WriteLine(string.Format("CustomerName:{0} Product:{1} Status:{2}", r.CustomerName, r.Products.Aggregate((a, b) => a + "," + b), r.StatusText));
                     });

                "演示返回表值的自定义函数".HightLightWriteLine();
                var query2 = from c in db.ufnGetCustomerInformation(1)
                             select c.FirstName + " " + c.LastName;
                Console.WriteLine(query2.Single());

                "演示使用存储过程代替系统自动生成的增删改SQL语句".HightLightWriteLine();
                db.ProductCategories.DeleteOnSubmit(db.ProductCategories.First());
                db.SubmitChanges();
            }
        }
    }
}
