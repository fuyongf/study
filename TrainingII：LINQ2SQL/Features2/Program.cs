﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using System.Threading;
using System.Data.Linq;
using System.Data.SqlClient;

namespace Features2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WindowWidth = 120;
            Console.BufferHeight = 1000;


            "演示时间戳列".HightLightWriteLine();
            using (NorthwindDataContext database = new NorthwindDataContext(new SqlConnection(
              @"Data Source=.;Initial Catalog=Northwind;Integrated Security=True")))
            {
                database.ExecuteCommand("delete from " + database.Mapping.GetTable(typeof(NorthwindProduct)).TableName);
                database.Products.InsertOnSubmit(new NorthwindProduct { ProductName = "asdads", CategoryId = 10 });
                database.SubmitChanges();
                var pro = database.Products.First();
                pro.ProductName = "产品名";
                database.SubmitChanges();
            }

            "初始化数据库".HightLightWriteLine();
            using (NorthwindDataContext database = new NorthwindDataContext(new SqlConnection(
                  @"Data Source=.;Initial Catalog=Northwind;Integrated Security=True")))
            {
                database.ExecuteCommand("delete from " + database.Mapping.GetTable(typeof(NorthwindCategory)).TableName);
                database.Categories.InsertOnSubmit(new NorthwindCategory
                {
                    CategoryName = "Name",
                    CategoryDesc = "Desc",
                });
                database.Categories.InsertOnSubmit(new NorthwindCategory
                {
                    CategoryName = "Name",
                    CategoryDesc = "Desc",
                });
                database.SubmitChanges();
            }

            "演示并发更新".HightLightWriteLine();
            //Action<Action<List<NorthwindCategory>>> updateCategory = updater =>
            //{
            //    using (NorthwindDataContext database = new NorthwindDataContext(new SqlConnection(
            //   @"Data Source=.;Initial Catalog=Northwind;Integrated Security=True")))
            //    {
            //        List<NorthwindCategory> categories = database.Categories.ToList();

            //        Thread.Sleep(2000);

            //        updater(categories);
            //        try
            //        {
            //            database.SubmitChanges();
            //        }
            //        catch (Exception ex)
            //        {
            //            Console.WriteLine(ex);
            //        }
            //    }
            //};


            Action<Action<List<NorthwindCategory>>> updateCategory = updater =>
            {
                using (NorthwindDataContext database = new NorthwindDataContext(new SqlConnection(
               @"Data Source=.;Initial Catalog=Northwind;Integrated Security=True")))
                {
                    List<NorthwindCategory> categories = database.Categories.ToList();

                    Thread.Sleep(2000);

                    updater(categories);

                    try
                    {
                        database.SubmitChanges(ConflictMode.ContinueOnConflict);
                    }
                    catch (ChangeConflictException)
                    {
                        foreach (ObjectChangeConflict conflict in database.ChangeConflicts)
                        {
                            Console.WriteLine(
                                "冲突行: ID = {0}.",
                                (conflict.Object as NorthwindCategory).Id);

                            foreach (MemberChangeConflict member in conflict.MemberConflicts)
                            {
                                Console.WriteLine(
                                    "列 [{0}] 的值有冲突，原始值：{1}，数据库当前值：{2}，当前值：{3}",
                                    member.Member.Name,
                                    member.OriginalValue,
                                    member.DatabaseValue, member.CurrentValue);
                                if (member.Member.Name == "CategoryName")
                                    member.Resolve(RefreshMode.KeepCurrentValues);// 所有更新以当前值为准
                                else if (member.Member.Name == "CategoryDesc")
                                    member.Resolve(RefreshMode.KeepChanges);// 原始的更新有效
                            }

                            //conflict.Resolve(RefreshMode.KeepCurrentValues); // 所有更新以当前值为准，默认
                            //conflict.Resolve(RefreshMode.KeepChanges); // 原始的更新有效，冲突的值以当前更新为准
                            //conflict.Resolve(RefreshMode.OverwriteCurrentValues); // 放弃当前更新

                        }

                        //database.ChangeConflicts.ResolveAll(RefreshMode.OverwriteCurrentValues);
                        database.SubmitChanges();

                        categories.ForEach(category => Console.WriteLine(category.CategoryName + " " + category.CategoryDesc));

                    }

                }
            };

            var t1 = new Thread(() => updateCategory(categories => categories.ForEach(category =>
                {
                    category.CategoryName = "Name1";
                    category.CategoryDesc = "Desc1";
                })));
            t1.Start();

            Thread.Sleep(1000);

            var t2 = new Thread(() => updateCategory(categories => categories.ForEach(category => category.CategoryName = "Name2")));
            t2.Start();

            t1.Join();
            t2.Join();

            "演示事务".HightLightWriteLine();
            try
            {
                using (NorthwindDataContext database = new NorthwindDataContext(new SqlConnection(
              @"Data Source=.;Initial Catalog=Northwind;Integrated Security=True")))
                {
                    database.Products.First().ProductName = DateTime.Now.ToString(); //回滚
                    database.Categories.First().CategoryName = new string('a', 100);
                    database.SubmitChanges();
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("{0}: {1}", exception.GetType(), exception.Message);


            }


            using (NorthwindDataContext database = new NorthwindDataContext(new SqlConnection(
          @"Data Source=.;Initial Catalog=Northwind;Integrated Security=True")))
            {
                Console.WriteLine(database.Products.First().ProductName);
            }

            Console.ReadLine();
        }
    }
}
