﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using System.Data.Linq;

namespace Features
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.BufferHeight = 1000;

            "演示延迟执行".HightLightWriteLine();
            IEnumerable<string> names = GetCategoryNames(16, 18);
            foreach (string name in names)
            {
                Console.WriteLine(name);
            }

            "演示级联延迟加载".HightLightWriteLine();
            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                db.ProductCategories.ToList().Take(8).ToList().ForEach(c =>
                {
                    Console.WriteLine(c.Name);
                    c.Products.ToList().Take(2).ToList().ForEach(p =>
                    {
                        Console.WriteLine(new string(' ', 5) + p.Name + "  " + p.ListPrice);
                    });
                });
            }

            "演示字段延迟加载".HightLightWriteLine();
            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                var customer = db.Customers.First();
                Console.WriteLine(customer.FirstName);
                Console.WriteLine(customer.PasswordHash);
            }

            "通过LoadWith来强制加载级联对象".HightLightWriteLine();
            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                DataLoadOptions options = new DataLoadOptions();
                options.LoadWith<ProductCategory>(category => category.Products);
                options.AssociateWith<ProductCategory>(category => category.Products.Where(p => p.ListPrice > 2000));
                db.LoadOptions = options;
                db.ProductCategories.ToList().Take(8).ToList().ForEach(c =>
                {
                    Console.WriteLine(c.Name);
                    c.Products.ToList().Take(2).ToList().ForEach(p =>
                    {
                        Console.WriteLine(new string(' ', 5) + p.Name + " " + p.ListPrice);
                    });
                });
            }


            "演示关闭延迟加载".HightLightWriteLine();
            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                DataLoadOptions options = new DataLoadOptions();
                options.LoadWith<Product>(category => category.ProductCategory);
                db.LoadOptions = options;
                db.DeferredLoadingEnabled = false;
                Product product = db.Products.First();
                try
                {
                    Console.WriteLine(product.ProductCategory.Name);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);// 没有设置级联加载又关闭了延迟加载
                }
            }

            Console.ReadLine();
        }

        static IEnumerable<string> GetCategoryNames(params int[] ids)
        {
            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                return db.ProductCategories
                    .Where(category => ids.Contains(category.ProductCategoryID))
                    .Select(category => category.Name).ToList(); // 注释ToList()试试看
            }

        }
    }
}
