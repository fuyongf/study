﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using System.Data.SqlClient;

namespace Overview
{
    class Program
    {
        static void Main(string[] args)
        {
            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                db.Mapping.GetTables().ToList().ForEach(t => Console.WriteLine(string.Format("{0} => {1}", t.TableName, t.RowType.Type.FullName)));
                db.Mapping.GetFunctions().ToList().ForEach(t => Console.WriteLine(string.Format("{0} => {1}", t.Name, t.Method.ToString())));

                var customer = db.Customers.FirstOrDefault();
                Console.WriteLine(customer.CustomerID + " " + customer.CompanyName);
                customer.CompanyName = "5173";
                db.SubmitChanges();

                Console.WriteLine(db.Customers.Where(c => c.CustomerAddresses.Any(add => add.AddressType == "Shipping")).Count());
            }

            using (NorthwindDataContext database = new NorthwindDataContext(new SqlConnection(
                @"Data Source=.;Initial Catalog=Northwind;Integrated Security=True")))
            {
                if (database.DatabaseExists())
                {
                    database.DeleteDatabase();
                }

                database.CreateDatabase();
                Console.WriteLine("创建数据库成功！");

                database.Log = Console.Out;

                var p = new NorthwindCategory
                {
                    CategoryName = "测试",
                    
                };

                database.Categories.InsertOnSubmit(p);
                database.SubmitChanges();
                Console.WriteLine(p.Id);

                database.Categories.DeleteOnSubmit(p);
                database.SubmitChanges();
            }
        }
    }
}
