﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;

namespace Usage
{
    class 组 : 演示
    {
        protected override void 操作()
        {
            QueryCount(db.Products.Select(p=>p.Color).Distinct());

            QueryCount(db.Products.Where(p=>p.Color == "Red").Union(db.Products.Where(p=>p.StandardCost <20)));

            QueryCount(db.Products.Where(p => p.Color == "Red").Concat(db.Products.Where(p => p.StandardCost < 20)));

            QueryCount(db.Products.Select(p=>p.ProductID).Intersect(db.SalesOrderDetails.Select(o=>o.ProductID)));

            QueryCount(db.Products.Select(p => p.ProductID).Except(db.SalesOrderDetails.Select(o => o.ProductID)));
        }
    }
}
