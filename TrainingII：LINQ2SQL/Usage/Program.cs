﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;

namespace Usage
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.BufferHeight = 1000;
            //using (限制 a = new 限制()) ;
           // using (投影 a = new 投影()) ;
           // using (排序 a = new 排序()) ;
            //using (组 a = new 组()) ;
            //using (聚合 a = new 聚合()) ;
            //using (分区 a = new 分区()) ;
            //using (限定 a = new 限定()) ;
            //using (元素 a = new 元素()) ;
            //using (分组 a = new 分组()) ;
            //using (连接 a = new 连接()) ;
            using (SqlMethods演示 a = new SqlMethods演示()) ;
            Console.ReadLine();
        }
    }

    abstract class 演示 : IDisposable
    {
        protected abstract void 操作();

        protected AdventureWorksDataContext db;

        protected virtual void QueryCount<T>(IQueryable<T> query)
        {
            Console.WriteLine("LINQ查询：" + query.Expression.ToString());
            Console.WriteLine("TO SQL查询：");
            try
            {
                Console.WriteLine("查询返回记录数：" + query.ToList().Count());
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine();
        }

        protected virtual void QueryFirst<T>(IQueryable<T> query)
        {
            Console.WriteLine("LINQ查询：" + query.Expression.ToString());
            Console.WriteLine("TO SQL查询：");
            try
            {
                Console.WriteLine("查询返回的第一条记录：" + query.ToList().First().ToString());
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public 演示()
        {
            db = new AdventureWorksDataContext();
            测试();
        }

        private void 测试()
        {
            var color = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("=================" + this.GetType().Name + "=================");
            Console.ForegroundColor = color;
            操作();         
            Console.ForegroundColor = color;
            Console.WriteLine();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
