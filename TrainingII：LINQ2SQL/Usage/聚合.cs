﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Usage
{
    class 聚合 : 演示
    {
        protected override void 操作()
        {
            db.SalesOrderDetails.LongCount();

            db.SalesOrderDetails.Count();

            db.SalesOrderDetails.Average(p=>p.UnitPrice);

            db.SalesOrderDetails.Sum(p => p.UnitPrice);

            db.SalesOrderDetails.Min(p => p.UnitPrice);

            db.SalesOrderDetails.Max(p => p.UnitPrice);
        }
    }
}
