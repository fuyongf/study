﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.SqlClient;

namespace Usage
{
    class SqlMethods演示 : 演示
    {
        protected override void 操作()
        {
            QueryCount(db.Products.Where(p => SqlMethods.DateDiffDay(p.SellStartDate, p.SellEndDate) > 10));

            QueryCount(db.Products.Where(p => !SqlMethods.Like(p.Name, "%p%")));
        }
    }
}
