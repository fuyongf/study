﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Usage
{
    class 限制 : 演示
    {
        protected override void 操作()
        {
            QueryCount(db.Products.Where(p => p.Color == "Red" || p.Color == "Black"));
            
            QueryCount(db.Products.Where(p => p.ProductNumber.StartsWith("BK-R68R")));

            QueryCount(db.Products.Where(p => new [] { 9,25,21 }.Contains(p.ProductModelID.Value)));

            QueryCount(db.Products.Where(p => p.SellEndDate != null));
        }
    }
}
