﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;

namespace Usage
{
    class 投影 : 演示
    {
        protected override void 操作()
        {
            QueryFirst(db.Products.Select(p => new { Name = p.Name, Model = p.ProductModel.Name }));

            QueryFirst(db.Products.Select(p => new NorthwindProduct { Id = p.ProductID, ProductName = p.Name, CategoryId = p.ProductCategoryID.Value }));

            QueryFirst(db.Products.Select(p => new { Name = p.Name, IsSolidColor = (p.Color != "Multi" && p.Color != null) }));

            QueryCount(db.Customers.Where(p=>p.Title == "Mr.").SelectMany(p => p.CustomerAddresses));
        }
    }
}
