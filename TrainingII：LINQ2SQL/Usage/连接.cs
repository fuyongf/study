﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;

namespace Usage
{
    class 连接 : 演示
    {
        protected override void 操作()
        {
            // 内连接
            var results = from category in db.ProductCategories
                          join product in db.Products on category.ProductCategoryID equals product.ProductCategoryID
                          select new
                          {
                              ProductName = product.Name,
                              ListPrice = product.ListPrice,
                              CategoryName = category.Name
                          };
            QueryCount(results);

            // 外连接
            var results2 = from category in db.ProductCategories
                           join product in db.Products on category.ProductCategoryID equals product.ProductCategoryID
                           into categories
                           from item in categories.DefaultIfEmpty()
                           select new
                           {
                               ProductName = item.Name ?? "N/A",
                               ListPrice = item == null ? 0 : item.ListPrice,
                               CategoryName = category.Name
                           };
            QueryCount(results2);

            // 子查询

            var result3 = from p in db.Products
                          let orderid = from o in db.SalesOrderDetails where o.OrderQty > 5 select o.ProductID
                          where orderid.Contains(p.ProductID)
                          select p.Name;

            QueryCount(result3);

        }
    }
}
