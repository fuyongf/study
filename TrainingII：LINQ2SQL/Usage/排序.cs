﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;

namespace Usage
{
    class 排序 : 演示
    {
        protected override void 操作()
        {
            QueryFirst(db.Products.OrderBy(p => p.ListPrice).OrderBy(p => p.StandardCost));

            QueryFirst(db.Products.OrderBy(p => p.ListPrice).ThenBy(p => p.StandardCost));

            QueryFirst(db.Products.OrderBy(p => p.ListPrice).ThenByDescending(p => p.StandardCost));
        }
    }
}
