﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Usage
{
    class 分区 : 演示
    {
        protected override void 操作()
        {
            QueryCount(db.Customers.Take(10));

            QueryCount(db.Customers.Skip(10));

            QueryCount(db.Customers.Skip(10).Take(10));
        }
    }
}
