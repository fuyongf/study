﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Usage
{
    class 分组 : 演示
    {
        protected override void 操作()
        {
            QueryFirst(db.Products.GroupBy(
                product => product.ProductCategoryID,
                (key, products) => new
                {
                    Key = key,
                    Count = products.Count()
                }));

            QueryFirst(db.Products.GroupBy(
                product => product.ProductCategoryID,
                (key, products) => new
                {
                    Key = key,
                    Count = products.Count()
                }).Where(g=>g.Count > 40));

            // groupby？
            IQueryable<IGrouping<string, string>> groups = db.Products.GroupBy(
                product => product.Name.Substring(0, 1),
                product => product.Name).Take(2); // 如果1w条记录呢？

            foreach (var group in groups)
            {
                Console.Write("Group {0}: ", group.Key);
                foreach (string productName in group)
                {
                    Console.Write("[{0}] ", productName);
                }

                Console.WriteLine();
            }
        }
    }
}
