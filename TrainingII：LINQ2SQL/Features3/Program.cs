﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using Common;
using System.ComponentModel;

namespace Features3
{
    public class NewTopic : Topic
    {
        public NewTopic()
        {
            base.TopicType = 0;
        }
    }

    public class Reply : Topic
    {
        public Reply()
        {
            base.TopicType = 1;
        }

        [Column(Name = "ParentTopic", DbType = "int", CanBeNull = false)]
        public int ParentTopic { get; set; }

    }

    [Table(Name = "Topics")]
    [InheritanceMapping(Code = 0, Type = typeof(NewTopic), IsDefault = true)]
    [InheritanceMapping(Code = 1, Type = typeof(Reply))]
    public class Topic
    {
        [Column(Name = "TopicID", DbType = "int identity", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
        public int TopicID { get; set; }

        [Column(Name = "TopicType", DbType = "tinyint", CanBeNull = false, IsDiscriminator = true)]
        protected int TopicType { get; set; }

        [Column(Name = "TopicTitle", DbType = "varchar(50)", CanBeNull = false)]
        public string TopicTitle { get; set; }

        [Column(Name = "TopicContent", DbType = "varchar(max)", CanBeNull = false)]
        public string TopicContent { get; set; }
    }

    [Table(Name = "Categories")]
    public class BoardCategory
    {

        [Column(Name = "CategoryID", DbType = "int identity", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
        public int CategoryID { get; set; }

        [Column(Name = "CategoryName", DbType = "varchar(50)", CanBeNull = false)]
        public string CategoryName { get; set; }

        private EntitySet<Board> _Boards;
        [Association(OtherKey = "BoardCategory", Storage = "_Boards")]

        public EntitySet<Board> Boards
        {
            get { return this._Boards; }

            set { this._Boards.Assign(value); }
        }

        public BoardCategory()
        {
            this._Boards = new EntitySet<Board>();
        }
    }

    [Table(Name = "Boards")]
    public class Board
    {
        [Column(Name = "BoardID", DbType = "int identity", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
        public int BoardID { get; set; }

        [Column(Name = "BoardName", DbType = "varchar(50)", CanBeNull = false)]
        public string BoardName { get; set; }

        [Column(Name = "BoardCategory", DbType = "int", CanBeNull = false)]
        public int BoardCategory { get; set; }

        private EntityRef<BoardCategory> _Category;
        [Association(ThisKey = "BoardCategory", Storage = "_Category")]

        public BoardCategory Category
        {
            get
            {
                return this._Category.Entity;
            }
            set
            {
                BoardCategory previousValue = this._Category.Entity;
                if (((previousValue != value) || (this._Category.HasLoadedOrAssignedValue == false)))
                {
                    if ((previousValue != null))
                    {
                        this._Category.Entity = null;
                        previousValue.Boards.Remove(this);
                    }
                    this._Category.Entity = value;
                    if ((value != null))
                    {
                        value.Boards.Add(this);
                        this.BoardCategory = value.CategoryID;
                    }
                    else
                    {
                        this.BoardCategory = 0;
                    }
                }
            }

        }

        public Board()
        {
            this._Category = default(EntityRef<BoardCategory>);
        }

    }


    public partial class BBSContext : DataContext
    {

        public Table<BoardCategory> BoardCategories;

        public Table<Board> Boards;

        public Table<Topic> Topics;

        public BBSContext(string connection) : base(connection) { this.Log = Console.Out; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            using (BBSContext db = new BBSContext("Data Source=.;Initial Catalog=BBS;Integrated Security=True"))
            {
                if (!db.DatabaseExists())
                    db.CreateDatabase();

                db.Mapping.GetTables().ToList().ForEach(table => db.ExecuteCommand("truncate table " + table.TableName));

                "初始化主题".HightLightWriteLine();
                var topic1 = new NewTopic
                {
                    TopicTitle = "干田是个好孩子",
                    TopicContent = "因为不挑食"
                };
                var topic2 = new NewTopic
                {
                    TopicTitle = "高工是个有钱人",
                    TopicContent = "因为他买了海景房",
                };
                db.Topics.InsertAllOnSubmit(new[] { topic1, topic2 });
                db.SubmitChanges();

                db.Topics.InsertOnSubmit(new Reply
                {
                    ParentTopic = topic1.TopicID,
                    TopicContent = "是啊！",
                    TopicTitle = "朱晔回复说"
                });

                db.Topics.InsertOnSubmit(new Reply
                {
                    ParentTopic = topic1.TopicID,
                    TopicContent = "同意！",
                    TopicTitle = "赵青回复说"
                });

                db.Topics.InsertOnSubmit(new Reply
                {
                    ParentTopic = topic2.TopicID,
                    TopicContent = "我想去参观！",
                    TopicTitle = "海峰回复说"
                });

                db.SubmitChanges();

                "初始化版块".HightLightWriteLine();

                BoardCategory database = new BoardCategory() { CategoryName = "Database" };
                BoardCategory language = new BoardCategory() { CategoryName = "Language" };
                db.BoardCategories.InsertOnSubmit(database);
                db.BoardCategories.InsertOnSubmit(language);
                db.SubmitChanges();

                Board oracle = new Board() { BoardName = "Oracle", Category = database };
                Board sqlserver = new Board() { BoardName = "SqlServer", Category = database };                          
                Board csharp = new Board() { BoardName = "c#", Category = language };
                Board cplusplus = new Board() { BoardName = "c++", Category = language };               

                db.SubmitChanges();

            }

            using (BBSContext db = new BBSContext("Data Source=.;Initial Catalog=BBS;Integrated Security=True"))
            {
                DataLoadOptions options = new DataLoadOptions();
                options.LoadWith<BoardCategory>(category => category.Boards);
                
                //db.LoadOptions = options;

                var query = from topic in db.Topics.OfType<NewTopic>()
                            orderby topic.TopicID descending
                            select new
                            {
                                Content = topic.TopicTitle + " - " + topic.TopicContent,
                                Replies = from reply in db.Topics.OfType<Reply>()
                                          where reply.ParentTopic == topic.TopicID
                                          select new
                                          {
                                              Content = reply.TopicTitle + " - " + reply.TopicContent
                                          }
                            };

                "开始查询，演示继承".HightLightWriteLine();
                query.ToList().ForEach(topic =>
                {
                    Console.WriteLine(topic.Content);
                    topic.Replies.ToList().ForEach(reply => Console.WriteLine(string.Concat(new string(' ', 10), reply.Content)));
                });

                "开始查询，演示关系".HightLightWriteLine();
                db.BoardCategories.ToList().ForEach(category =>
                    {
                        Console.WriteLine(category.CategoryName);
                        category.Boards.ToList().ForEach(board => Console.WriteLine(string.Concat(new string(' ', 10), board.BoardName)));
                    });
            }

            Console.ReadLine();
        }
    }
}
