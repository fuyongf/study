﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using System.Data.Linq;
using System.Data.SqlClient;

namespace Usage2
{
    class Program
    {
        static void Main(string[] args)
        {
            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                var a = db.Products.OrderBy(p => p.ProductID).ToList();
                var b = db.Products.OrderByDescending(p => p.ProductID).ToList();
                object.ReferenceEquals(a.First(), b.Last()).ToString().HightLightWriteLine("演示实体标识1");
            }

            var aa = new List<Product>();
            var bb = new List<Product>();
            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                aa = db.Products.OrderBy(p => p.ProductID).ToList();
            }

            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                bb = db.Products.OrderByDescending(p => p.ProductID).ToList();
            }

            object.ReferenceEquals(aa.First(), bb.Last()).ToString().HightLightWriteLine("演示实体标识2");

            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                var a = db.Products.OrderBy(p => p.ProductID).Select(p => new { Id = p.ProductID }).ToList();
                var b = db.Products.OrderByDescending(p => p.ProductID).Select(p => new { Id = p.ProductID }).ToList();
                object.ReferenceEquals(a.First(), b.Last()).ToString().HightLightWriteLine("演示实体标识3");
            }

            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                var a = db.tests.OrderBy(p => p.id).ToList();
                var b = db.tests.OrderByDescending(p => p.id).ToList();
                object.ReferenceEquals(a.First(), b.Last()).ToString().HightLightWriteLine("演示实体标识4");
            }

            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                var a = db.Products.First();
                a.Name.HightLightWriteLine("原来的产品名");
                a.Name = "修改后的产品名";
                a.Name.HightLightWriteLine("修改后的产品名");
                var b = db.Products.GetOriginalEntityState(a);
                b.Name.HightLightWriteLine("再获取原来的产品名");

                var c = new Product() { Name = "新产品" };
                c.Name.HightLightWriteLine("原来的产品名");
                db.Products.Attach(c); // 注释这行试试看
                c.Name = "修改后的新产品名";
                a.Name.HightLightWriteLine("修改后的产品名");
                (db.Products.GetOriginalEntityState(c) ?? new Product() { Name = "N/A" }).Name.HightLightWriteLine("通过Attach挂载离线对象支持状态跟踪");
            }

            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                ProductCategory category = new ProductCategory();

                var productsOfCategory2 = db.Products.Where(
                    item => item.ProductCategoryID == 18);

                productsOfCategory2.ToList().ForEach(p => p.ProductCategory = category);
                category.Products.Count.ToString().HightLightWriteLine("测试级联跟踪");

                ChangeSet changeSet = db.GetChangeSet();
                string.Format("Update : {0}, Insert : {1}, Delete : {2}", changeSet.Updates.Count, changeSet.Inserts.Count, changeSet.Deletes.Count)
                    .HightLightWriteLine("测试更改集合");

                changeSet.Updates.First().GetType().Name.HightLightWriteLine("更新的对象之一：");
                changeSet.Inserts.First().GetType().Name.HightLightWriteLine("新增的对象之一：");

                // 随意对对象进行复杂操作，最后LINQ2SQL为您生成各种查询
            }

            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                ProductCategory category = new ProductCategory()
                {
                    Name = "新类别" + Guid.NewGuid().ToString(),
                    ModifiedDate = DateTime.Now,
                    rowguid = Guid.NewGuid()
                };
                Product product1 = new Product() 
                {
                    Name = "新产品1" + Guid.NewGuid().ToString(),
                    ModifiedDate = DateTime.Now,
                    SellStartDate = DateTime.Now,
                    ProductNumber = Guid.NewGuid().ToString().Substring(0, 25),
                    rowguid = Guid.NewGuid()
                    
                };
                Product product2 = new Product()
                {
                    Name = "新产品2" + Guid.NewGuid().ToString(),
                    ModifiedDate = DateTime.Now,
                    SellStartDate = DateTime.Now,
                    ProductNumber = Guid.NewGuid().ToString().Substring(0, 25),
                    rowguid = Guid.NewGuid()

                };
                category.Products.AddRange(new [] {product1, product2});
                db.ProductCategories.InsertOnSubmit(category);

                db.SubmitChanges();

                "插入数据后会自动为实体同步主键".HightLightWriteLine();
                Console.WriteLine(category.ProductCategoryID);
                Console.WriteLine(product1.ProductID);
                Console.WriteLine(product1.ProductCategoryID);

            }

            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                var pd = db.Products.First();
                string.Format("{0} -> {1}", pd.ListPrice, ++pd.ListPrice).HightLightWriteLine("更新之前必须先有SELECT");
                db.SubmitChanges();
            }

            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                "因此删除和更新可能就会包含N条SQL语句，还要注意删除会根据外键调整次序".HightLightWriteLine();
                db.ProductCategories.DeleteAllOnSubmit(db.ProductCategories.Where(c => c.Name.Contains("新类别")));
                db.Products.DeleteAllOnSubmit(db.Products.Where(c => c.Name.Contains("新产品")));
                db.SubmitChanges();

            }

            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                "演示从datareader填充对象".HightLightWriteLine();
                db.Connection.Open();
                var cmd = new SqlCommand("SELECT TOP 1 *  FROM [SalesLT].[Customer] order by newid()", db.Connection as SqlConnection);
                var reader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                var result = db.Translate<Customer>(reader);
                Console.WriteLine(result.First().CompanyName);

            }

            using (AdventureWorksDataContext db = new AdventureWorksDataContext())
            {
                "演示为SELECT操作加上WITH NOLOCK".HightLightWriteLine();
                var query = db.Customers.Take(10);
                db.ExecuteQuery<Customer>(query, true).ToList();
            }

            Console.ReadLine();
        }
    }
}
