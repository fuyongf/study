﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.Data.Common;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using System.Data.Linq.Mapping;
using System.Reflection;

namespace Common
{
    partial class AdventureWorksDataContext
    {
        [Function(Name = "dbo.sp_multiresultset")]
        [ResultType(typeof(Customer))]
        [ResultType(typeof(CustomerAddress))]
        public IMultipleResults sp_multiresultset2()
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
            return (IMultipleResults)(result.ReturnValue);
        }

        partial void OnCreated()
        {
            this.CommandTimeout = 60;
            this.Log = Console.Out;
        }
    }

    partial class Product
    {
        public override string ToString()
        {
            return string.Format("ID:{0} Name:{1}", this.ProductID, this.Name);
        }
    }

    public static class Ext
    {
        public static void HightLightWriteLine(this string content)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(content);
            Console.ResetColor();
        }

        public static void HightLightWriteLine(this string content, string desc)
        {
            Ext.HightLightWriteLine(desc + " : " + content);
        }

        public static List<T> ExecuteQuery<T>(
        this DataContext dataContext, IQueryable query, bool withNoLock)
        {
            DbCommand command = dataContext.GetCommand(query, withNoLock);

            dataContext.OpenConnection();

            using (DbDataReader reader = command.ExecuteReader())
            {
                return dataContext.Translate<T>(reader).ToList();
            }
        }

        private static void OpenConnection(this DataContext dataContext)
        {
            if (dataContext.Connection.State == ConnectionState.Closed)
            {
                dataContext.Connection.Open();
            }
        }


        private static Regex s_withNoLockRegex =
            new Regex(@"(] AS \[t\d+\])", RegexOptions.Compiled);

        private static string AddWithNoLock(string cmdText)
        {
            IEnumerable<Match> matches =
                s_withNoLockRegex.Matches(cmdText).Cast<Match>()
                .OrderByDescending(m => m.Index);
            foreach (Match m in matches)
            {
                int splitIndex = m.Index + m.Value.Length;
                cmdText =
                    cmdText.Substring(0, splitIndex) + " WITH (NOLOCK)" +
                    cmdText.Substring(splitIndex);
            }

            return cmdText;
        }

        private static SqlCommand GetCommand(
            this DataContext dataContext, IQueryable query, bool withNoLock)
        {
            SqlCommand command = (SqlCommand)dataContext.GetCommand(query);

            if (withNoLock)
            {
                command.CommandText = AddWithNoLock(command.CommandText);
            }

            Console.WriteLine(command.CommandText);

            return command;
        }

    }
}
