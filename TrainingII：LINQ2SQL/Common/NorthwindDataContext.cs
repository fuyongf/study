﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data;

namespace Common
{
    [Table(Name = "Categories")]
    public class NorthwindCategory
    {
        [Column(DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated=true)]
        public int Id { get; set; }

        [Column(DbType = "NVarChar(15) NOT NULL")]
        public string CategoryName { get; set; }

        [Column(DbType = "NVarChar(15)")]
        public string CategoryDesc { get; set; }

        [Association(Name = "Category_Products",
            ThisKey = "Id", OtherKey = "CategoryId")]
        public EntitySet<NorthwindProduct> Products { get; set; }

        public NorthwindCategory()
        {
            Products = new EntitySet<NorthwindProduct>();
        }
    }

    [Table(Name = "Products")]
    public class NorthwindProduct
    {
        [Column(DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }

        [Column(DbType = "NVarChar(40) NOT NULL")]
        public string ProductName { get; set; }

        [Column(DbType = "Int")]
        public int CategoryId { get; set; }

        [Column(AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)]
        public Binary Version { get; set; }

    }

    [Database(Name = "Northwind")]
    public class NorthwindDataContext : DataContext
    {
        public NorthwindDataContext(IDbConnection connection)
            : base(connection)
        {
            this.Log = Console.Out;
        }

        public Table<NorthwindCategory> Categories
        {
            get
            {
                return this.GetTable<NorthwindCategory>();
            }
        }

        public Table<NorthwindProduct> Products
        {
            get
            {
                return this.GetTable<NorthwindProduct>();
            }
        }
    }
}
