﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace ConfigurationStudy
{
    class Program
    {
        static void Main(string[] args)
        {
            var section = (CustomerConfigSection)ConfigurationManager.GetSection("mailConfig");
            Console.WriteLine(section.App);
            foreach (CustomerConfigElement s in section.MailChanneles)
            {
                Console.WriteLine(string.Format("name={0},userName={1}, password={2}, mailServer={3}, port={4}", s.Name, s.UserName, s.Password, s.MailServer, s.Port));
            }
            Console.Read();

        }
    }
}
