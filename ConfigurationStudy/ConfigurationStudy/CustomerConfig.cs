﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace ConfigurationStudy
{
    public class CustomerConfigSection:ConfigurationSection
    {
        [ConfigurationProperty("app", IsRequired = true)]
        public string App { get { return this["app"] as string; } }

        [ConfigurationProperty("mailChanneles", IsDefaultCollection= false)]
        public CustomerConfigElementCollection MailChanneles 
        { 
            get { return this["mailChanneles"] as CustomerConfigElementCollection; }
        }
    }

    public class CustomerConfigElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name 
        {
            get { return this["name"] as string; }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("userName",IsRequired=true)]
        public string UserName 
        { 
            get { return this["userName"] as string; }
            set { this["userName"] = value; }
        }

        [ConfigurationProperty("password", IsRequired = true)]
        public string Password
        {
            get { return this["password"] as string; }
            set { this["password"] = value; }
        }

        [ConfigurationProperty("mailServer", IsRequired = true)]
        public string MailServer
        {
            get { return this["mailServer"] as string; }
            set { this["mailServer"] = value; }
        }

        [ConfigurationProperty("port", IsRequired = true)]
        public int Port
        {
            get { return (int)this["port"]; }
            set { this["port"] = value; }
        }
    }


    public class CustomerConfigElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new CustomerConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CustomerConfigElement)element).Name;
        }

        #region 使用默认<add />不需要重写，如需要指定子节点名称，则需要重写以下行为，告知子节点名称。<channel name="1" userName="a" password="aa" mailServer="aaa" port="1"/>
        protected override string ElementName
        {
            get { return "channel"; }
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }
        #endregion
    }
}
