﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="MVCFlow.Core" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%= Html.Encode(ViewData["Message"]) %></h2>
    <p id="content">
        
    </p>
    
     <script language"javascript" type="text/javascript">
            $().ready(function() {    
                    $.ajaxSetup({ cache: false });                                   
                    $.get("<%=ResolveUrl("~/Home/GetFileContent") %>", function(data) {
                        $("#content").html(data.Content);
                    });
            });
        </script>
</asp:Content>
