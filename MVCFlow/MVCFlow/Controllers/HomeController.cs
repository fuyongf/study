﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCFlow.Core;
using MVCFlow.Models;

namespace MVCFlow.Controllers
{
    [TestActionFilterOnController]
    public class HomeController : TestController
    {
        [TestActionFilterOnAction]
        public ActionResult Index(TestModel model)
        {
            TempData["Message"] = ViewData["Message"] = "MVC执行流程如下：";
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult LogOnUserControl()
        {
            return PartialView();
        }

        public ActionResult GetFileContent()
        {
            return Json(new { Content = System.IO.File.ReadAllText(Server.MapPath("~/log.txt")) }, JsonRequestBehavior.AllowGet);
        }
    }
}
