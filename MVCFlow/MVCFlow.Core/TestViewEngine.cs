﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MVCFlow.Core
{
    public class TestViewEngine : WebFormViewEngine
    {
        protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath)
        {
            Helper.LogBegin(partialPath);
            return Helper.LogEnd(new TestView(partialPath));
        }

        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
        {
            Helper.LogBegin(viewPath, masterPath);
            return Helper.LogEnd(new TestView(viewPath, masterPath));
        }

        protected override bool FileExists(ControllerContext controllerContext, string virtualPath)
        {
            Helper.LogBegin(virtualPath);
            return Helper.LogEnd(base.FileExists(controllerContext, virtualPath));
        }

        public override ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache)
        {
            Helper.LogBegin(partialViewName, useCache ? "useCache" : "");
            return Helper.LogEnd(base.FindPartialView(controllerContext, partialViewName, useCache));
        }
        public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
        {
            Helper.LogBegin(viewName, masterName, useCache ? "useCache" : "");
            return Helper.LogEnd(base.FindView(controllerContext, viewName, masterName, useCache));
        }

        public override void ReleaseView(ControllerContext controllerContext, IView view)
        {
            Helper.LogBegin(view.GetType().Name);
            base.ReleaseView(controllerContext, view);
        }
    }
}
