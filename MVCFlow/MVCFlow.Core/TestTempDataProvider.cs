﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MVCFlow.Core
{
    public class TestTempDataProvider : ITempDataProvider
    {
        #region ITempDataProvider Members

        public IDictionary<string, object> LoadTempData(ControllerContext controllerContext)
        {
            Helper.LogBegin();
            return null;
        }

        public void SaveTempData(ControllerContext controllerContext, IDictionary<string, object> values)
        {
            Helper.LogBegin();
        }

        #endregion
    }
}
