﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web;

namespace MVCFlow.Core
{
    public class TestMvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            Helper.LogBegin();

            AreaRegistration.RegisterAllAreas();
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new TestViewEngine());
            ControllerBuilder.Current.SetControllerFactory(typeof(TestControllerFactory));
            ValueProviderFactories.Factories.Add(new TestValueProviderFactory());
            ModelBinders.Binders.Clear();
            ModelBinders.Binders.Add(typeof(TestModel), new TestModelBinder());

            TestRoute route = new TestRoute("{controller}/{action}",
                new RouteValueDictionary(new
                {
                    controller = "Home",
                    action = "Index",
                }), new RouteValueDictionary(new { action = new TestRouteConstraint() }),
                new TestMvcRouteHandler());
            RouteTable.Routes.Add("Default", route);
        }

        public override void Init()
        {
            base.Init();
            base.EndRequest += new EventHandler(TestMvcApplication_EndRequest);
        }

        private void TestMvcApplication_EndRequest(object sender, EventArgs e)
        {
            Helper.WriteFlow();
        }
    }
}
