﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections;
using System.Web.Mvc;
using System.Diagnostics;
using System.Web.Routing;
using System.IO;

namespace MVCFlow.Core
{
    public static class Helper
    {
        private readonly static string httpContextItemKey = "Flow";

        public static void LogBegin(params string[] parms)
        {
            var context = new HttpContextWrapper(HttpContext.Current);
            context.LogBegin(parms);
        }

        public static T LogEnd<T>(T result)
        {
            var context = new HttpContextWrapper(HttpContext.Current);
            return context.LogEnd(result);
        }

        public static void LogEnd()
        {
            LogEnd<object>(null);
        }

        private static void LogBegin(this HttpContextBase context, params string[] parms)
        {
            if (!context.Items.Contains(httpContextItemKey))
                context.Items.Add(httpContextItemKey, new Queue<string>());
            StackTrace strackTrace = new StackTrace();
            int skip = 0;
            var currentMedhod = strackTrace.GetFrame(skip).GetMethod();
            while (currentMedhod.Name.Contains("LogBegin"))
            {
                skip++;
                currentMedhod = strackTrace.GetFrame(skip).GetMethod();
            }
            var methodName = currentMedhod.Name;
            var typeName = currentMedhod.DeclaringType.Name;
            var queue = context.Items[httpContextItemKey] as Queue<string>;
            queue.Enqueue(string.Format(Enumerable.Repeat("-", strackTrace.GetFrames().Length).Aggregate((a, b) => a + b) + "<font color=\"red\">开始执行</font><strong>{0}</strong> 类的 {1} 方法", typeName, methodName)
                + (parms != null && parms.Length > 0 ? string.Format("传入了：<strong>{0}</strong>", string.Join(",", parms.Where(p => !string.IsNullOrEmpty(p)).ToArray())) : "")
                + string.Format(" -- {0}", DateTime.Now.ToString("hh:mm:ss:ffff")));
        }

        private static T LogEnd<T>(this HttpContextBase context, T result)
        {
            if (!context.Items.Contains(httpContextItemKey))
                context.Items.Add(httpContextItemKey, new Queue<string>());
            StackTrace strackTrace = new StackTrace();
            int skip = 0;
            var currentMedhod = strackTrace.GetFrame(skip).GetMethod();
            while (currentMedhod.Name.Contains("LogEnd"))
            {
                skip++;
                currentMedhod = strackTrace.GetFrame(skip).GetMethod();
            }
            var methodName = currentMedhod.Name;
            var typeName = currentMedhod.DeclaringType.Name;
            var queue = context.Items[httpContextItemKey] as Queue<string>;
            queue.Enqueue(string.Format(Enumerable.Repeat("-", strackTrace.GetFrames().Length).Aggregate((a, b) => a + b) + "<font color=\"green\">结束执行</font><strong>{0}</strong> 类的 {1} 方法", typeName, methodName)
                + (result != null ? string.Format("返回了：<strong>{0}</strong>", result is ValueType ? result.ToString() : result.GetType().Name) : "")
                + string.Format(" -- {0}", DateTime.Now.ToString("hh:mm:ss:ffff")));

            return result;
        }


        public static void WriteFlow()
        {
            var context = HttpContext.Current;
            var data = context.Items[httpContextItemKey] as Queue<string>;
            var fileName = context.Server.MapPath("~/log.txt");
            if (data != null)
            {
                if (File.Exists(fileName)) File.Delete(fileName);
                File.AppendAllText(fileName, "<ul>", Encoding.UTF8);
                foreach (var item in data)
                {
                    File.AppendAllText(fileName, string.Format("<li>{0}</li>", item), Encoding.UTF8);
                }
                File.AppendAllText(fileName, "</ul>", Encoding.UTF8);
            }
        }
    }
}
