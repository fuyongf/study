﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVCFlow.Core
{
    public class TestController : Controller
    {
        protected override IActionInvoker CreateActionInvoker()
        {
            Helper.LogBegin();
            return Helper.LogEnd(new TestActionInvoker());
        }

        protected override ITempDataProvider CreateTempDataProvider()
        {
            Helper.LogBegin();
            return Helper.LogEnd(new TestTempDataProvider());
        }

        protected override void ExecuteCore()
        {
            Helper.LogBegin();
            base.ExecuteCore();
        }

        protected override void Dispose(bool disposing)
        {
            Helper.LogBegin();
            base.Dispose(disposing);
        }

        protected override void Execute(RequestContext requestContext)
        {
            Helper.LogBegin();
            base.Execute(requestContext);
        }

        protected override PartialViewResult PartialView(string viewName, object model)
        {
            Helper.LogBegin();

            if (model != null)
            {
                ViewData.Model = model;
            }

            var result = new TestPartialViewResult
            {
                ViewName = viewName,
                ViewData = ViewData,
                TempData = TempData
            };

            return Helper.LogEnd(result);
        }

        protected override ViewResult View(string viewName, string masterName, object model)
        {
            Helper.LogBegin();

            if (model != null)
            {
                ViewData.Model = model;
            }

            var result = new TestViewResult
            {
                ViewName = viewName,
                MasterName = masterName,
                ViewData = ViewData,
                TempData = TempData
            };

            return Helper.LogEnd(result);
        }

        protected override void Initialize(RequestContext requestContext)
        {
            Helper.LogBegin();
            base.Initialize(requestContext);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            Helper.LogBegin(actionName);
            base.HandleUnknownAction(actionName);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Helper.LogBegin();
            base.OnActionExecuted(filterContext);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Helper.LogBegin();
            base.OnActionExecuting(filterContext);
        }

        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            Helper.LogBegin();
            base.OnAuthorization(filterContext);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Helper.LogBegin();
            base.OnException(filterContext);
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            Helper.LogBegin();
            base.OnResultExecuted(filterContext);
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            Helper.LogBegin();
            base.OnResultExecuting(filterContext);
        }


    }
}
