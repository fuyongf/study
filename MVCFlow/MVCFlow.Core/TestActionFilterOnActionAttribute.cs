﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MVCFlow.Core
{
    public class TestActionFilterOnActionAttribute : ActionFilterAttribute, IExceptionFilter, IResultFilter, IActionFilter, IAuthorizationFilter
    {
        #region IAuthorizationFilter Members

        void IAuthorizationFilter.OnAuthorization(AuthorizationContext filterContext)
        {
            Helper.LogBegin();
        }

        #endregion

        #region IActionFilter Members

        void IActionFilter.OnActionExecuted(ActionExecutedContext filterContext)
        {
            Helper.LogBegin();
        }

        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            Helper.LogBegin();
        }

        #endregion

        #region IResultFilter Members

        void IResultFilter.OnResultExecuted(ResultExecutedContext filterContext)
        {
            Helper.LogBegin();
        }

        void IResultFilter.OnResultExecuting(ResultExecutingContext filterContext)
        {
            Helper.LogBegin();
        }

        #endregion

        #region IExceptionFilter Members

        void IExceptionFilter.OnException(ExceptionContext filterContext)
        {
            Helper.LogBegin();
            filterContext.ExceptionHandled = true;
        }

        #endregion
    }
}
