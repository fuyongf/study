﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MVCFlow.Core
{
    public class TestView : WebFormView
    {
        public TestView(string viewPath) : base(viewPath) { }
        public TestView(string viewPath, string masterPath) : base(viewPath, masterPath) { }
        public override void Render(ViewContext viewContext, System.IO.TextWriter writer)
        {
            Helper.LogBegin();
            base.Render(viewContext, writer);
        }
    }
}
