﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVCFlow.Core
{
    public class TestControllerFactory : DefaultControllerFactory
    {
        public override IController CreateController(RequestContext requestContext, string controllerName)
        {
            Helper.LogBegin(controllerName);
            return base.CreateController(requestContext, controllerName);
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            Helper.LogBegin(controllerType.Name);
            return Helper.LogEnd(base.GetControllerInstance(requestContext, controllerType));
        }

        protected override Type GetControllerType(RequestContext requestContext, string controllerName)
        {
            Helper.LogBegin(controllerName);
            return Helper.LogEnd(base.GetControllerType(requestContext, controllerName));
        }

        public override void ReleaseController(IController controller)
        {
            Helper.LogBegin();
            base.ReleaseController(controller);
        }
    }
}
