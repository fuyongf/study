﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web;
using System.Globalization;

namespace MVCFlow.Core
{
    public class TestValueProviderFactory : ValueProviderFactory
    {
        public override IValueProvider GetValueProvider(ControllerContext controllerContext)
        {
            return Helper.LogEnd(new TestValueProvider(controllerContext.HttpContext.Request.Cookies));
        }

        private class TestValueProvider : IValueProvider
        {
            private readonly HttpCookieCollection _cookieCollection;

            public TestValueProvider(HttpCookieCollection cookieCollection)
            {
                _cookieCollection = cookieCollection;
            }

            public bool ContainsPrefix(string prefix)
            {
                return Helper.LogEnd(_cookieCollection[prefix] != null);
            }

            public ValueProviderResult GetValue(string key)
            {
                HttpCookie cookie = _cookieCollection[key];
                return Helper.LogEnd(cookie != null ? new TestValueProviderResult(cookie.Value, cookie.Value, CultureInfo.CurrentUICulture) : null);
            }
        }
    }
}
