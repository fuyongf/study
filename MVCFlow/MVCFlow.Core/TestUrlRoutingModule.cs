﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Routing;

namespace MVCFlow.Core
{
    public class TestUrlRoutingModule : UrlRoutingModule
    {
        public override void PostMapRequestHandler(System.Web.HttpContextBase context)
        {
            Helper.LogBegin();
            base.PostMapRequestHandler(context);
        }

        public override void PostResolveRequestCache(System.Web.HttpContextBase context)
        {
            Helper.LogBegin();
            base.PostResolveRequestCache(context);
        }
    }
}
