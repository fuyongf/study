﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MVCFlow.Core
{
    public class TestPartialViewResult : PartialViewResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            Helper.LogBegin();
            base.ExecuteResult(context);
        }

        protected override ViewEngineResult FindView(ControllerContext context)
        {
            Helper.LogBegin();
            return Helper.LogEnd(base.FindView(context));
        }
    }
}
