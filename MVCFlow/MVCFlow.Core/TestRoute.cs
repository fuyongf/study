﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Routing;

namespace MVCFlow.Core
{
   public class TestRoute : Route
    {
       public TestRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints, IRouteHandler routeHandler)
           : base(url, defaults, constraints, routeHandler)
       {
           
       }
       public override RouteData GetRouteData(System.Web.HttpContextBase httpContext)
       {
           Helper.LogBegin();
           return Helper.LogEnd(base.GetRouteData(httpContext));
       }

       public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
       {
           Helper.LogBegin();
           return Helper.LogEnd(base.GetVirtualPath(requestContext, values));
       }

       protected override bool ProcessConstraint(System.Web.HttpContextBase httpContext, object constraint, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
       {
           Helper.LogBegin();
           return Helper.LogEnd(base.ProcessConstraint(httpContext, constraint, parameterName, values, routeDirection));
       }
    }
}
