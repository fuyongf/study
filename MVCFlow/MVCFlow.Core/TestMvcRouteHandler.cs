﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web;

namespace MVCFlow.Core
{
    public class TestMvcRouteHandler : MvcRouteHandler
    {
        protected override IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            Helper.LogBegin();
            return Helper.LogEnd(new TestMvcHandler(requestContext));
        }
    }
}
