﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MVCFlow.Core
{
    public class TestActionInvoker : ControllerActionInvoker
    {
        protected override ActionResult CreateActionResult(ControllerContext controllerContext, ActionDescriptor actionDescriptor, object actionReturnValue)
        {
            Helper.LogBegin();
            return Helper.LogEnd(base.CreateActionResult(controllerContext, actionDescriptor, actionReturnValue));
        }

        protected override ActionDescriptor FindAction(ControllerContext controllerContext, ControllerDescriptor controllerDescriptor, string actionName)
        {
            Helper.LogBegin(actionName);
            return Helper.LogEnd(base.FindAction(controllerContext, controllerDescriptor, actionName));
        }

        protected override ControllerDescriptor GetControllerDescriptor(ControllerContext controllerContext)
        {
            Helper.LogBegin();
            return Helper.LogEnd(base.GetControllerDescriptor(controllerContext));
        }

        protected override FilterInfo GetFilters(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        {
            Helper.LogBegin();
            return Helper.LogEnd(base.GetFilters(controllerContext, actionDescriptor));
        }

        protected override object GetParameterValue(ControllerContext controllerContext, ParameterDescriptor parameterDescriptor)
        {
            Helper.LogBegin();
             return Helper.LogEnd( base.GetParameterValue(controllerContext, parameterDescriptor));
        }

        protected override IDictionary<string, object> GetParameterValues(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        {
            Helper.LogBegin();
            return Helper.LogEnd( base.GetParameterValues(controllerContext, actionDescriptor));
        }

        public override bool InvokeAction(ControllerContext controllerContext, string actionName)
        {
            Helper.LogBegin();
            return Helper.LogEnd( base.InvokeAction(controllerContext, actionName));
        }

        protected override ActionResult InvokeActionMethod(ControllerContext controllerContext, ActionDescriptor actionDescriptor, IDictionary<string, object> parameters)
        {
            Helper.LogBegin();
            return Helper.LogEnd( base.InvokeActionMethod(controllerContext, actionDescriptor, parameters));
        }

        protected override ActionExecutedContext InvokeActionMethodWithFilters(ControllerContext controllerContext, IList<IActionFilter> filters, ActionDescriptor actionDescriptor, IDictionary<string, object> parameters)
        {
            Helper.LogBegin();
            return Helper.LogEnd( base.InvokeActionMethodWithFilters(controllerContext, filters, actionDescriptor, parameters));
        }

        protected override void InvokeActionResult(ControllerContext controllerContext, ActionResult actionResult)
        {
            Helper.LogBegin();
            base.InvokeActionResult(controllerContext, actionResult);
        }

        protected override ResultExecutedContext InvokeActionResultWithFilters(ControllerContext controllerContext, IList<IResultFilter> filters, ActionResult actionResult)
        {
            Helper.LogBegin(actionResult.GetType().Name);
            return Helper.LogEnd( base.InvokeActionResultWithFilters(controllerContext, filters, actionResult));
        }

        protected override AuthorizationContext InvokeAuthorizationFilters(ControllerContext controllerContext, IList<IAuthorizationFilter> filters, ActionDescriptor actionDescriptor)
        {
            Helper.LogBegin();
            return Helper.LogEnd( base.InvokeAuthorizationFilters(controllerContext, filters, actionDescriptor));
        }

        protected override ExceptionContext InvokeExceptionFilters(ControllerContext controllerContext, IList<IExceptionFilter> filters, Exception exception)
        {
            Helper.LogBegin();
            return Helper.LogEnd(base.InvokeExceptionFilters(controllerContext, filters, exception));
        }
    }
}
