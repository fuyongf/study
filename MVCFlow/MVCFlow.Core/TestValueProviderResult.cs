﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Globalization;

namespace MVCFlow.Core
{
    public class TestValueProviderResult : ValueProviderResult
    {
        public TestValueProviderResult(object rawValue, string attemptedValue, CultureInfo culture)
            : base(rawValue, attemptedValue, culture) { }
        public override object ConvertTo(Type type, System.Globalization.CultureInfo culture)
        {
            Helper.LogBegin(type.Name);
            return base.ConvertTo(type, culture);
        }
    }
}
