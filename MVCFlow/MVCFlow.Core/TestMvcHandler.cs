﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web;

namespace MVCFlow.Core
{
    public class TestMvcHandler : MvcHandler
    {
        public TestMvcHandler(RequestContext requestContext)
            : base(requestContext)
        {
        }

        protected override void AddVersionHeader(HttpContextBase httpContext)
        {
            Helper.LogBegin();
            base.AddVersionHeader(httpContext);
        }

        protected override IAsyncResult BeginProcessRequest(HttpContextBase httpContext, AsyncCallback callback, object state)
        {
            httpContext.Response.Cookies.Add(new System.Web.HttpCookie("Test", "Cookie"));
            Helper.LogBegin();
            return base.BeginProcessRequest(httpContext, callback, state);
        }

        protected override void EndProcessRequest(IAsyncResult asyncResult)
        {
            Helper.LogBegin();
            base.EndProcessRequest(asyncResult);
        }
    }
}
