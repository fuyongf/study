﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("start");
            for (var i = 0; i < 500; i++)
            {
                if (i!=0 &&i % 100 == 0)
                {
                    Thread.Sleep(10000);
                }
                new Thread(Press).Start(i);
                //new Thread(new Program().Index).Start(i);
            }
            Console.WriteLine("wait");
            Console.Read();
        }
        public static void Press(object j)
        {
            //Console.WriteLine("j=" + j);
            HttpWebRequest request = WebRequest.Create("http://114.55.252.157:8085/api/press/Index") as HttpWebRequest;
            request.Method = "GET";
            request.Timeout = 600000;
            HttpWebResponse myResponse = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(myResponse.GetResponseStream(), Encoding.UTF8);
            var content = int.Parse(reader.ReadToEnd());
            Console.WriteLine("index=" + content);
        }

        static int count = 0;
        //内核堵塞
        static ManualResetEvent mre = new ManualResetEvent(false);
        // GET: Login
        public void Index(object obj)
        {
           
            var num = Interlocked.Increment(ref count);         
            var c = 50;
            if (count < c)
            {
                mre.WaitOne();
            }
            else if (c == count)
            {
                mre.Set();
            }
            //lock (obj)
            //{
            //    if (count < c)
            //    {
            //        mre.WaitOne();
            //    }
            //    else if (c == count)
            //    {
            //        mre.Set();
            //    }
            //}

            //return Thread.CurrentThread.ManagedThreadId;
            Console.WriteLine(num);
        }
    }
}
