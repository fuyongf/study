﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace PressTest.Controllers
{
    public class PressController : ApiController
    {
        [HttpGet]
        public string OK()
        {
            int wn = 0;
            int io = 0;
            int at = 0;
            int ait = 0;
            ThreadPool.GetMaxThreads(out at, out ait);
            ThreadPool.GetAvailableThreads(out wn, out io);
            return $"aw={wn}, io={io},totalC={at},totalIO={ait},已用worker数={at- wn}";
        }
        //每次测试都会重启iis
        static int count = 0;
        //内核堵塞
        static ManualResetEvent mre = new ManualResetEvent(false);
        static object obj = new object();
        /// <summary>
        /// 压测监控IIS请求数，
        /// 在线程堵塞的情况下线程池中的活动工作线程数=当前请求数。
        /// 为啥大部分时候当前请求数大于线程池活动工作线程数，
        /// 计数器采样是有延迟，
        /// 而线程池线程处理完请求后就复用再处理其他请求复用，线程数不会增加。
        /// iis对同一客户端的请求频率有限制。
        /// 不能用基元构造堵，即自旋，因为线程池不会直接起新线程
        /// </summary>
        /// <returns></returns>
        // GET: Login
        [HttpGet]
        public int Index()
        {          
            
            var num = Interlocked.Increment(ref count);
            Fancy.Basic.Logging.Log.Info($"ManagedThreadId={Thread.CurrentThread.ManagedThreadId},count={count}, clientIp={HttpContext.Current.Request.UserHostAddress}");
            var c = 990;
            if (count < c)
            {
                mre.WaitOne();
            }
            else if (c == count)
            {
                mre.Set();
            }
            return num;
        }

    }
}
